# Car Shop Application
[Barz Sebastian Mihai]

## Introducere
Am ales ca pentru acest proiect construiesc o aplicatie pentru o companie sau o persoana care vrea sa vanda masini. Aceasta va avea doua tipuri de utilizatori, useri si admini. Userii au acces la lista de masini din magazin, toate detaliile acestora si le pot achizitiona. Adminii sunt cei ce gestioneaza masinile. Acestia adauga masinile in baza de date, pot modifica datele sau sterge o masina. Aplicatia foloseste serviciile REST, scrisa in jimbajul JAVA, utilizand mediul de programare IntelliJ IDEA.

## Cerinte
Aplicatia foloseste framework-ul Spring Boot, respecta principiile SOLID si foloseste serviciile REST. Pentru baza de date am ales PostgreSQL si am creat-o folosind pgAdmin. Aplicatia finala urmeaza sa fie una Web, avand o pagina de Log In, unde utilizatorii normali sa se poata loga folosind un user si o parola. Apoi acestia pot vizualiza o lista cu anunturi, fiecare anunt avand un buton dedicat pentru achizitionarea masinii. Lista poate fi filtrata dupa mai multe preferinte(model, pret, etc.). Adminul este in principiu tot un utilizator, se va loga cu credentialele lui, dar este singurul care are acces si poate modifica lista de masini din magazin. Cand adminul va introduce o masina noua in lista, userii vor primi o notificare. Acest lucru va trebui implementat pe viitor, utilizand paternul Observer.

## Implementare
Partea de back-end a aplicatiei este pregatita, am reusit sa o implementez folosind 5 pachete care ma ajuta sa respect cerintele functionale ale acesteia: controllers, dtos, entities, repositories si servicies. Acestea sunt indeajuns pentru a mentine aplicatia intr-o arhitectura pe mai multe layere. Endpoint-ul testat in Postman este cel pentru masini, avand 4 operatii functionale: GET, POST, PUT si DELETE. Acesta se noteaza cu "/ca"r, unde se adauga, modifica si vizualizeaza masini si functioneaza perfect, masina este adaugata in baza de date si toate operatiile se pot efectua corect. Urmeaza sa testez si celelalte endpoint-uri, dar fiind destul de asemanatoare, nu cred ca vor fi probleme. 

## Endpoints

    /car: aici au loc operatiile CRUD pentru adaugare si de afisare a tuturor masinilor.
    
    /car/{id}: cu ajutorul id-ului se poate sterge, modifica sau afisa o anumita masina.
    
    /login: este endpointul unde se face logarea userului dupa credentiale: username si parola
    
    /person: este asemanator cu endpointul de la masini. Pe /person se  face adaugarea unui user nou si afisarea tuturor userilor. Aici se pot adauga atat admini, cat si useri, deoarece functia aceasta este aleasa cand se face un cont nou.
    
    /person/{id}: cu ajutorul id-ului unei persoane se pot modifica datele acestuia, se poate sterge contul sau se pot afisa informatiile aferente.

## Diagrama Deployment

![deployment_diagram](https://ibb.co/829fJTm)

## Diagrama Observer Pattern

![Observer_pattern_diagram](https://ibb.co/7Yk4KWd)

## Testare
Am implementat 3 teste, unul pentru o inserare obisnuita si inca 2 care testeaza clasele care valideaza datele de intrare. Utilizatorii inserati in baza de date nu pot avea campul de adresa lasat null sau sa fie minori. Acestea testeaza controllerele pentru persoana si validatorii pentru varsta. Testele functioneaza corect, iar pe viitor am sa dezvolt testare si pentru clasere din pachetul services. 
Acestea ne ofera siguranta ca applicatia functioneaza corect si toate modulele sunt legate corect.

## Imbunatatiri pe viitor
Aplicatia va fi in final una web, partea de back-end fiind aproape gata de deploy. Va trebui sa construiesc un front-end care contine o pagina de log in, iar apoi pe baza credentialelor vei fi redirectionat ori catre pagina adminului, unde se poate modifica lista de masini, ori spre pagina de user normal, unde se pot vizualiza si achizitiona. Inca o imbunatatire va fi pattern-ul Observer, care ma va ajuta sa trimit notificari userilor cand o masina se adauga in lista.
