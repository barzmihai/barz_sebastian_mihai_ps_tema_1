package carshop.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import carshop.CarshopTestConfig;
import carshop.dtos.PersonDetailsDTO;
import carshop.services.AdminService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class PersonControllerUnitTest extends CarshopTestConfig {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AdminService service;

    /**
     * test care verifica un insert simplu, care trebuie sa returneze statusul CREATED
     * @throws Exception
     */
    @Test
    public void insertPersonTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        PersonDetailsDTO personDTO = new PersonDetailsDTO("John", "Somewhere Else street", 22);
        personDTO.setEmail("mock@email.com");
        personDTO.setUsername("mockUsername");
        personDTO.setPassword("mockPassword");

        mockMvc.perform(post("/person")
                .content(objectMapper.writeValueAsString(personDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

    /**
     * test care verifica validatorul pentru varsta
     * persoana nu ar trebui sa poata fi introdusa, deoarece este minor
     * @throws Exception
     */
    @Test
    public void insertPersonTestFailsDueToAge() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        PersonDetailsDTO personDTO = new PersonDetailsDTO("John", "Somewhere Else street", 17);

        mockMvc.perform(post("/person")
                .content(objectMapper.writeValueAsString(personDTO))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    /**
     * test care verifica daca o persoana poate fi introdusa cu adresa nula
     * @throws Exception
     */
    @Test
    public void insertPersonTestFailsDueToNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        PersonDetailsDTO personDTO = new PersonDetailsDTO("John", null, 17);

        mockMvc.perform(post("/person")
                .content(objectMapper.writeValueAsString(personDTO))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }
}
