package carshop.services;

import carshop.CarshopTestConfig;
import carshop.entities.Person;
import carshop.repositories.PersonRepository;
import carshop.services.PersonService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import carshop.dtos.PersonDTO;
import carshop.dtos.PersonDetailsDTO;
import carshop.dtos.UserCredentialsDTO;
import carshop.entities.UserRole;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.assertEquals;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class PersonServiceTests extends CarshopTestConfig {

    @InjectMocks
    PersonService personService;

    @Mock
    PersonRepository repo;

    @Test
    public void testFindByName() {

        Person person = new Person();
        person.setName("John");
        person.setAddress("street");
        person.setEmail("dasd@gmail.com");
        person.setAge(22);

        when(repo.findByName(anyString())).thenReturn(java.util.Optional.of(person));
    }

    @Test
    public void testInsert(){

        Person person = new Person();
        person.setId(UUID.fromString("5048264c-d904-40b7-9110-574ebb36b48b"));
        person.setName("John");
        person.setAddress("street");
        person.setEmail("dasd@gmail.com");
        person.setAge(22);

        when(repo.save(person)).thenReturn(person);

    }
}
