package carshop.repositories;

import carshop.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import carshop.entities.UserRole;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Clasa care face legatura dintre logica de domeniu si accesul la
 * baza de date. Efectueaza operatii pe baza de date, se pot crea
 * query-uri foarte simplu.
 */

public interface AdminRepository extends JpaRepository<Person, UUID> {

    @Transactional
    Optional<Person> findByName(String name);

    @Transactional
    void deleteByName(String name);

    @Query(value = "SELECT p " +
            "FROM Person p " +
            "WHERE p.userCredentials.role = 0 ")

    List<Person> findPersonByRole(UserRole userRole);

}
