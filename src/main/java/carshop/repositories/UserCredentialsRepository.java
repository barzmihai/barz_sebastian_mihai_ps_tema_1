package carshop.repositories;

import carshop.entities.UserCredentials;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Clasa care face legatura dintre logica de domeniu si accesul la
 * baza de date. Efectueaza operatii pe baza de date, se pot crea
 * query-uri foarte simplu.
 */

public interface UserCredentialsRepository extends JpaRepository<UserCredentials, UUID> {

    UserCredentials findByUsernameAndPassword(String username, String password);

}
