package carshop.repositories;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;

import java.io.File;
import java.io.IOException;

public interface Report {
    File createReport(String name) throws IOException;
    Document createReportPdf(String name) throws IOException, DocumentException;
}
