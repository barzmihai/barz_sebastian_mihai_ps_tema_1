package carshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import carshop.entities.Person;
import carshop.entities.UserRole;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Clasa care face legatura dintre logica de domeniu si accesul la
 * baza de date. Efectueaza operatii pe baza de date, se pot crea
 * query-uri foarte simplu.
 */

public interface PersonRepository extends JpaRepository<Person, UUID> {

    @Transactional
    Optional<Person> findByName(String name);

    @Transactional
    void deleteByName(String name);

    @Transactional
    Optional<Person> findPersonById(UUID id);

    @Query(value = "SELECT p " +
            "FROM Person p " +
            "WHERE p.name = :name " +
            "AND p.age >= 60  ")
    Optional<Person> findSeniorsByName(@Param("name") String name);

    @Query(value = "SELECT p " +
            "FROM Person p " +
            "WHERE p.userCredentials.role = 2 ")

    @Transactional
    List<Person> findPersonByRole(UserRole userRole);

}
