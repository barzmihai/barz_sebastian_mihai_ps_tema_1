package carshop.repositories;

import carshop.entities.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Clasa care face legatura dintre logica de domeniu si accesul la
 * baza de date. Efectueaza operatii pe baza de date, se pot crea
 * query-uri foarte simplu.
 */

public interface CarRepository extends JpaRepository<Car, UUID> {

    @Transactional
    Optional<Car> findByBrand(String brand);

    @Transactional
    void deleteByBrand(String brand);

    @Transactional
    void deleteById(UUID id);

    @Override
    List<Car> findAll();

}
