package carshop.dtos;

import carshop.entities.UserRole;

import java.util.UUID;

/**
 * Clasa de tip Data Transfer Object,  este folosita pentru a
 * transmite informatii intre procele aplicatiei.
 */

public class UserCredentialsDTO {

    private UUID id;

    private String username;

    private String password;

    private UserRole role;

    public UserCredentialsDTO() {
    }

    public UserCredentialsDTO(String username, String password, UserRole role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
