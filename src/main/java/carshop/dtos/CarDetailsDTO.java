package carshop.dtos;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

/**
 * Clasa de tip Data Transfer Object,  este folosita pentru a
 * transmite informatii intre procele aplicatiei.
 */

public class CarDetailsDTO {

    private UUID id;
    @NotNull
    private String brand;
    @NotNull
    private String model;
    @NotNull
    private String engine;
    @NotNull
    private String type;
    @NotNull
    private int price;

    public CarDetailsDTO() {
    }

    public CarDetailsDTO(UUID id, @NotNull String brand, @NotNull String model, @NotNull String engine, @NotNull String type, @NotNull int price) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.engine = engine;
        this.type = type;
        this.price = price;
    }

    public CarDetailsDTO(@NotNull String brand, @NotNull String model, @NotNull String engine, @NotNull String type, @NotNull int price) {
        this.brand = brand;
        this.model = model;
        this.engine = engine;
        this.type = type;
        this.price = price;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, price);
    }
}
