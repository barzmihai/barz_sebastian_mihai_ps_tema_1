package carshop.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;
import java.util.UUID;

/**
 * Clasa de tip Data Transfer Object,  este folosita pentru a
 * transmite informatii intre procele aplicatiei.
 */

public class CarDTO extends RepresentationModel<CarDTO> {
    private UUID id;
    private String brand;
    private String model;
    private String engine;
    private String type;
    private int price;

    public CarDTO() {
    }

    public CarDTO(UUID id, String brand, String model, String engine, String type, int price) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.engine = engine;
        this.type = type;
        this.price = price;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, price);
    }
}
