package carshop.dtos.builders;

import carshop.dtos.CarDTO;
import carshop.dtos.CarDetailsDTO;
import carshop.entities.Car;

public class CarBuilder {

    private CarBuilder() {
    }

    public static CarDTO toCarDTO(Car car) {
        return new CarDTO(car.getId(), car.getBrand(), car.getModel(), car.getEngine(), car.getType(), car.getPrice());
    }

    public static CarDetailsDTO toCarDetailsDTO(Car car) {
        return new CarDetailsDTO(car.getId(), car.getBrand(), car.getModel(), car.getEngine(), car.getType(), car.getPrice());
    }

    public static Car toEntity(CarDetailsDTO carDetailsDTO) {
        return new Car(carDetailsDTO.getBrand(), carDetailsDTO.getModel(), carDetailsDTO.getEngine(), carDetailsDTO.getType(), carDetailsDTO.getPrice());
    }
}
