package carshop.controllers;


import carshop.dtos.PersonDTO;
import carshop.dtos.PersonDetailsDTO;
import carshop.services.AdminService;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * Clasa care primeste requesturi si le trimite mai departe la service,
 * functioneaza ca o poarta intre intrarea aplicatiei si logica de domeniu,
 * decide ce sa faca cu intrarea si cum sa transmita raspunsul.
 * @RestController este o versiune specializată a controlerului.
 * Include adnotările @Controller și @ResponseBody și, ca urmare, simplifică implementarea controlerului.
 */

@RestController
@CrossOrigin
@RequestMapping(value = "/admin")
public class AdminController {

    private final AdminService adminService;

    @Autowired
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

}
