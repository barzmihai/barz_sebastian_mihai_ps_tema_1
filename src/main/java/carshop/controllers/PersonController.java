package carshop.controllers;


import carshop.dtos.PersonDTO;
import carshop.dtos.PersonDetailsDTO;
import com.itextpdf.text.DocumentException;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import carshop.services.PersonService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * Clasa care primeste requesturi si le trimite mai departe la service,
 * functioneaza ca o poarta intre intrarea aplicatiei si logica de domeniu,
 * decide ce sa faca cu intrarea si cum sa transmita raspunsul.
 * @RestController este o versiune specializată a controlerului.
 * Include adnotările @Controller și @ResponseBody și, ca urmare, simplifică implementarea controlerului.
 */

@RestController
@CrossOrigin
@RequestMapping(value = "/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    /**
     * apeleaza metoda din service care gaseste toate persoanele
     * @return OK status
     */
    @GetMapping()
    public ResponseEntity<List<PersonDTO>> getPersons() {
        List<PersonDTO> dtos = personService.findPersons();
        for (PersonDTO dto : dtos) {
            Link personLink = linkTo(methodOn(PersonController.class)
                    .getPerson(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    /**
     * apeleaza metoda din service care insereza o persoana
     * @param personDTO
     * @return CREATED status
     */
    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody PersonDetailsDTO personDTO) {
        UUID personID = personService.insert(personDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    /**
     * apeleaza metoda din service care cauta o persoana dupa id-ul acesteia
     * @param personId
     * @return OK status
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<PersonDetailsDTO> getPerson(@PathVariable("id") UUID personId) {
        PersonDetailsDTO dto = personService.findPersonById(personId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    /**
     * apeleaza metoda din service care face update la persoana cu numele transmis
     * @param id
     * @param personDTO
     * @return OK status
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity<PersonDetailsDTO> updatePerson(@PathVariable("id") UUID id, @Valid @RequestBody PersonDetailsDTO personDTO) {
        return new ResponseEntity<>(personService.update(id, personDTO), HttpStatus.OK);
    }

    /**
     * apeleaza metoda din service care sterge persoana cu numele transmis prin variabila name
     * @param name
     * @return
     */
    @DeleteMapping(value = "/{name}")
    public ResponseEntity<JSONObject> deletePerson(@PathVariable("name") String name){
        String s = personService.delete(name);
        JSONObject obj = new JSONObject();
        obj.put("delete", s);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping(value = "/pdf")
    public void reportpdf() throws IOException, DocumentException {
        personService.creareRaport("pdf","masini");
    }

    @GetMapping(value = "/txt")
    public void reporttxt() throws IOException, DocumentException {
        personService.creareRaport("txt","masini");
    }

    @GetMapping(value = "/html")
    public void reporthtml() throws IOException, DocumentException {
        personService.creareRaport("html","masini");
    }

}
