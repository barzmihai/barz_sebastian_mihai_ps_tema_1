package carshop.controllers;


import carshop.dtos.CarDTO;
import carshop.dtos.CarDetailsDTO;
import carshop.services.CarService;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLOutput;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


/**
 * Clasa care primeste requesturi si le trimite mai departe la service,
 * functioneaza ca o poarta intre intrarea aplicatiei si logica de domeniu,
 * decide ce sa faca cu intrarea si cum sa transmita raspunsul.
 * @RestController este o versiune specializată a controlerului.
 * Include adnotările @Controller și @ResponseBody și, ca urmare, simplifică implementarea controlerului.
 */

@RestController
@CrossOrigin
@RequestMapping(value = "/car")
public class CarController {

    private final CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    /**
     * @return toate masinile
     */
    @GetMapping()
    public ResponseEntity<List<CarDTO>> getCar() {
        List<CarDTO> dtos = carService.findCar();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    /**
     * apeleaza metoda de insert din service cu parametrul carDTO
     * @param carDTO
     * @return carId, CREATED status
     */
    @PostMapping()
    public ResponseEntity<UUID> insertCar(@Valid @RequestBody CarDetailsDTO carDTO) {
        UUID carID = carService.insert(carDTO);
        return new ResponseEntity<>(carID, HttpStatus.CREATED);
    }

    /**
     * apeleaza o metoda din service care cauta masina dupa id-ul acesteia
     * @param carId
     * @return OK status
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<CarDetailsDTO> getCar(@PathVariable("id") UUID carId) {
        CarDetailsDTO dto = carService.findCarById(carId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    /**
     * apeleza metoda din service care face update la o masina cu numele din variabila name
     * @param id
     * @param carDTO
     * @return OK status
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity<CarDetailsDTO> updateCar(@PathVariable("id") UUID id, @Valid @RequestBody CarDetailsDTO carDTO) {
        return new ResponseEntity<>(carService.update(id, carDTO), HttpStatus.OK);
    }

    /**
     * apeleaza metoda din service care sterge o masina dupa numele acesteia
     * @param id
     * @return Ok status
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<JSONObject> deleteCar(@PathVariable("id") UUID id){
        String s = carService.delete(id);
        JSONObject obj = new JSONObject();
        obj.put("delete", s);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

}
