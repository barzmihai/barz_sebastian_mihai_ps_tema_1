package carshop.controllers;

import carshop.dtos.UserCredentialsDTO;
import carshop.entities.UserCredentials;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import carshop.entities.UserRole;
import carshop.services.UserCredentialsService;

/**
 * Clasa care primeste requesturi si le trimite mai departe la service,
 * functioneaza ca o poarta intre intrarea aplicatiei si logica de domeniu,
 * decide ce sa faca cu intrarea si cum sa transmita raspunsul.
 * @RestController este o versiune specializată a controlerului.
 * Include adnotările @Controller și @ResponseBody și, ca urmare, simplifică implementarea controlerului.
 */

@RestController
@CrossOrigin
@RequestMapping(value = "/")
public class UserCredentialsController {

    private final UserCredentialsService userCredentialsService;

    public UserCredentialsController(UserCredentialsService userCredentialsService) {
        this.userCredentialsService = userCredentialsService;
    }

    /**
     * La log in se verifica credentialele userului
     * @param userCredentialsDTO
     * @return FORBIDDEN status daca nu e in regula
     */
    @PostMapping("login")
    public ResponseEntity<UserRole> login(@RequestBody UserCredentialsDTO userCredentialsDTO) {
        UserCredentials userCredentials = userCredentialsService.login(userCredentialsDTO);
        if (userCredentials != null) {
            return ResponseEntity.ok(userCredentials.getRole());
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }


}
