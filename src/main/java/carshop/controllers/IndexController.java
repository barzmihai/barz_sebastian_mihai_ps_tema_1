package carshop.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Clasa care primeste requesturi si le trimite mai departe la service,
 * functioneaza ca o poarta intre intrarea aplicatiei si logica de domeniu,
 * decide ce sa faca cu intrarea si cum sa transmita raspunsul.
 * @RestController este o versiune specializată a controlerului.
 * Include adnotările @Controller și @ResponseBody și, ca urmare, simplifică implementarea controlerului.
 */

@RestController
@CrossOrigin
public class IndexController {

    @GetMapping(value = "/")
    public ResponseEntity<String> getStatus() {
        return new ResponseEntity<>("City APP Service is running...", HttpStatus.OK);
    }
}
