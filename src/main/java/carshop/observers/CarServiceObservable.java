package carshop.observers;

import carshop.entities.Car;

import java.util.Observable;

public class CarServiceObservable extends Observable implements IObservable<Car> {

    @Override
    public void dataChanged(Car payload) {
        //System.out.println(this.countObservers());
        this.setChanged();
        this.notifyObservers(payload);
    }

}
