package carshop.observers;

import java.util.Observable;
import java.util.Observer;

public abstract class NotificationObserver implements Observer {

    public abstract NotificationObserver getObserver();

    protected abstract void handleDataChange(Object arg);

    @Override
    public void update(Observable o, Object arg) {
        handleDataChange(arg);
    }
}
