package carshop.observers;

public interface IObservable<T> {

    void dataChanged(T payload);
}
