package carshop.services;

import carshop.repositories.Report;

public abstract class ReportService {

    abstract Report factoryMethod(String type);
}
