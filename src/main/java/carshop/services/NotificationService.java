package carshop.services;

import carshop.observers.NotificationObserver;
import org.springframework.stereotype.Service;

import java.util.Observable;

@Service
public class NotificationService extends NotificationObserver {

    @Override
    public NotificationObserver getObserver() {
        return this;
    }

    @Override
    protected void handleDataChange(Object arg) {
        System.out.println("Notificare! " + arg.toString());
    }

}
