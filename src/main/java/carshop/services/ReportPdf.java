package carshop.services;

import carshop.repositories.Report;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ReportPdf implements Report {

    public ReportPdf()
    {
    }

    @Override
    public Document createReportPdf(String name) throws DocumentException, FileNotFoundException {
        Document myFile=new Document();
        String fileLocation = new File("src").getAbsolutePath()+"\\"+name+".pdf";
        PdfWriter.getInstance(myFile,new FileOutputStream(fileLocation));
        return myFile;
    }

    @Override
    public File createReport(String name) throws IOException
    {
        return null;
    }


}
