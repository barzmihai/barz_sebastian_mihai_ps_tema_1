package carshop.services;

import carshop.dtos.CarDetailsDTO;
import carshop.dtos.PersonDTO;
import carshop.dtos.PersonDetailsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import carshop.controllers.handlers.exceptions.model.ResourceNotFoundException;
import carshop.dtos.builders.PersonBuilder;
import carshop.entities.Person;
import carshop.entities.UserCredentials;
import carshop.entities.UserRole;
import carshop.repositories.AdminRepository;
import carshop.repositories.UserCredentialsRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Clasa de tip Service este cea care are acces la cele de tip Repository,
 * aici este dezvoltata logica de domeniu si se face accesul la baza de date.
 */

@Service
public class AdminService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final UserCredentialsRepository userCredentialsRepository;
    private final AdminRepository adminRepository;

    @Autowired
    public AdminService(AdminRepository adminRepository, UserCredentialsRepository userCredentialsRepository) {
        this.adminRepository = adminRepository;
        this.userCredentialsRepository = userCredentialsRepository;
    }

    public PersonDetailsDTO findAdminById(UUID id) {
        Optional<Person> adminOptional = adminRepository.findById(id);
        if (!adminOptional.isPresent()) {
            LOGGER.error("Admin with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return PersonBuilder.toPersonDetailsDTO(adminOptional.get());
    }

    public PersonDetailsDTO update(String name, PersonDetailsDTO adminDTO) {
        Optional<Person> adminOptional = adminRepository.findByName(name);
        if (!adminOptional.isPresent()) {
            LOGGER.error("Admin with name {} was not found in db", name);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with name: " + name);
        }
        Person d = adminOptional.get();
        d.setAddress(adminDTO.getAddress());
        d.setAge(adminDTO.getAge());
        d.setName(adminDTO.getName());
        adminRepository.save(d);
        return PersonBuilder.toPersonDetailsDTO(d);
    }

    public String delete(String name){
        adminRepository.deleteByName(name);
        return "Delete succsessful";
    }
}
