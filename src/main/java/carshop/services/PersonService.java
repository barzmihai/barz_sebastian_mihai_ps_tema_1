package carshop.services;

import carshop.dtos.PersonDTO;
import carshop.dtos.PersonDetailsDTO;
import carshop.entities.Car;
import carshop.entities.Person;
import carshop.entities.UserCredentials;
import carshop.repositories.CarRepository;
import carshop.repositories.PersonRepository;
import carshop.repositories.Report;
import com.itextpdf.text.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import carshop.controllers.handlers.exceptions.model.ResourceNotFoundException;
import carshop.dtos.builders.PersonBuilder;
import carshop.entities.UserRole;
import carshop.repositories.UserCredentialsRepository;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Clasa de tip Service este cea care are acces la cele de tip Repository,
 * aici este dezvoltata logica de domeniu si se face accesul la baza de date.
 */

@Service
public class PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final PersonRepository personRepository;
    private final CarRepository carRepository;
    private final UserCredentialsRepository userCredentialsRepository;

    @Autowired
    public PersonService(PersonRepository personRepository, CarRepository carRepository, UserCredentialsRepository userCredentialsRepository) {
        this.personRepository = personRepository;
        this.carRepository = carRepository;
        this.userCredentialsRepository = userCredentialsRepository;
    }

    /**
     * metoda care cauta toate persoanele din db
     * @return
     */
    public List<PersonDTO> findPersons() {
        List<Person> personList = personRepository.findAll();
        System.out.println("dasdasdasd     " + personList);
        return personList.stream()
                .map(PersonBuilder::toPersonDTO)
                .collect(Collectors.toList());
    }

    /**
     * metoda care cauta o persoana dupa id
     * @param id
     * @return PersonDetailsDTO
     */
    public PersonDetailsDTO findPersonById(UUID id) {
        Optional<Person> prosumerOptional = personRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return PersonBuilder.toPersonDetailsDTO(prosumerOptional.get());
    }

    /**
     * metoda care insereaza o persoana in db
     * @param personDTO
     * @return personID
     */
    @Transactional
    public UUID insert(PersonDetailsDTO personDTO) {
        UserCredentials userCredentials = new UserCredentials(
                personDTO.getEmail(),
                personDTO.getPassword(),
                UserRole.USER);
        userCredentials = userCredentialsRepository.save(userCredentials);

        Person person = PersonBuilder.toEntity(personDTO);
        person.setUserCredentials(userCredentials);
        person = personRepository.save(person);
        LOGGER.debug("Person with id {} was inserted in db", person.getId());
        return person.getId();
    }

    /**
     * metoda care face update la o persoana cu numele din variabila name
     * @param id
     * @param personDTO
     * @return PersonDetailsDTO
     */
    public PersonDetailsDTO update(UUID id, PersonDetailsDTO personDTO) {
        Optional<Person> prosumerOptional = personRepository.findPersonById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        Person p = prosumerOptional.get();
        p.setAddress(personDTO.getAddress());
        p.setAge(personDTO.getAge());
        p.setName(personDTO.getName());
        p.setEmail(personDTO.getEmail());
        personRepository.save(p);
        return PersonBuilder.toPersonDetailsDTO(p);
    }

    /**
     * metoda care sterge persoana dupa nume
     * @param name
     * @return "Delete succsessful" message
     */
    public String delete(String name){
        personRepository.deleteByName(name);
        return "Delete succsessful";
    }

    public void creareRaport(String type, String numeFisier) throws IOException, DocumentException, IOException, DocumentException {

        List<Car> masini;
        ReportService factory= new ReportCreatorService();
        Report report=factory.factoryMethod(type);
        masini = carRepository.findAll();

        if (type.equals("pdf")) {
            Document myDoc=report.createReportPdf(numeFisier);
            myDoc.open();
            Font font = FontFactory.getFont(FontFactory.COURIER,12, BaseColor.BLACK);
            if (masini.isEmpty())
            {
                System.out.println("Baza de date goala");
            } else {
                for (Car c : masini) {
                    Paragraph par = new Paragraph(c.toString(), font);
                    myDoc.add(par);
                }
            }
            myDoc.close();
        } else {
            File myFile = report.createReport(numeFisier);
            FileWriter myWriter = new FileWriter(myFile);

            if (masini.isEmpty()) {
                System.out.println("Baza de date goala");
            } else {
                for (Car c : masini) {
                    myWriter.write(c.toString());
                    myWriter.write('\n');
                }
            }
            myWriter.close();
        }
    }
}
