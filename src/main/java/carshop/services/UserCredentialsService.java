package carshop.services;

import carshop.dtos.UserCredentialsDTO;
import carshop.entities.UserCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import carshop.repositories.UserCredentialsRepository;

/**
 * Clasa de tip Service este cea care are acces la cele de tip Repository,
 * aici este dezvoltata logica de domeniu si se face accesul la baza de date.
 */

@Service
public class UserCredentialsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserCredentialsService.class);
    private final UserCredentialsRepository userCredentialsRepository;

    @Autowired
    public UserCredentialsService(UserCredentialsRepository userCredentialsRepository) {
        this.userCredentialsRepository = userCredentialsRepository;
    }

    public UserCredentials login(UserCredentialsDTO userCredentialsDTO) {
        return userCredentialsRepository.findByUsernameAndPassword(
                userCredentialsDTO.getUsername(),
                userCredentialsDTO.getPassword());
    }
}
