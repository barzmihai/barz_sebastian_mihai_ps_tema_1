package carshop.services;

import carshop.repositories.Report;

public class ReportCreatorService extends ReportService{

    public ReportCreatorService()
    {
    }

    @Override
    public Report factoryMethod(String type)
    {
        if (("html").equals(type))
        {
            return new ReportHtml();
        }
        if (("txt").equals(type))
        {
            return new ReportTxt();
        }
        if (("pdf").equals(type))
        {
            return new ReportPdf();
        }
        return null;
    }
}
