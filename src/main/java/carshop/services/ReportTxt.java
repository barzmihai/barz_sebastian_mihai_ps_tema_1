package carshop.services;

import carshop.repositories.Report;
import com.itextpdf.text.Document;

import java.io.File;
import java.io.IOException;

public class ReportTxt implements Report {

    public ReportTxt()
    {

    }

    @Override
    public File createReport(String name) throws IOException{
        File myFile=null;

        try{
            String fileLocation = new File("src").getAbsolutePath()+"\\"+name+".txt";
            myFile = new File(fileLocation);
            if (myFile.createNewFile())
            {
                System.out.println("Fisierul a fost creat.");
            }

        }catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return myFile;
    }

    @Override
    public Document createReportPdf( String name) throws IOException
    {
        return null;
    }

}
