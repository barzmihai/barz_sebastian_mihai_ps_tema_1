package carshop.services;

import carshop.dtos.CarDTO;
import carshop.dtos.CarDetailsDTO;
import carshop.dtos.builders.CarBuilder;
import carshop.observers.CarServiceObservable;
import carshop.observers.IObservable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import carshop.controllers.handlers.exceptions.model.ResourceNotFoundException;
import carshop.entities.Car;
import carshop.repositories.CarRepository;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Clasa de tip Service este cea care are acces la cele de tip Repository,
 * aici este dezvoltata logica de domeniu si se face accesul la baza de date.
 */

@Service
public class CarService extends CarServiceObservable {
    private static final Logger LOGGER = LoggerFactory.getLogger(CarService.class);
    private final CarRepository carRepository;

    @Autowired
    public CarService(CarRepository carRepository, NotificationService notificationService) {
        this.carRepository = carRepository;
        this.addObserver(notificationService.getObserver());
        System.out.println(this.countObservers());
    }

    /**
     * metoda care afiseaza toate masinile
     * @return
     */
    public List<CarDTO> findCar() {
        List<Car> carList = carRepository.findAll();
        return carList.stream()
                .map(CarBuilder::toCarDTO)
                .collect(Collectors.toList());
    }

    /**
     * metoda care cauta in db masina cu un anume ID
     * @param id
     * @return CarDetailsDTO
     */
    public CarDetailsDTO findCarById(UUID id) {
        Optional<Car> carOptional = carRepository.findById(id);
        if (!carOptional.isPresent()) {
            LOGGER.error("Car with id {} was not found in db", id);
            throw new ResourceNotFoundException(Car.class.getSimpleName() + " with id: " + id);
        }
        return CarBuilder.toCarDetailsDTO(carOptional.get());
    }

    /**
     * insereaza o masina in db
     * @param carDTO
     * @return carID
     */
    public UUID insert(CarDetailsDTO carDTO) {
        Car car = CarBuilder.toEntity(carDTO);
        car = carRepository.save(car);
        LOGGER.debug("Car with id {} was inserted in db", car.getId());
        dataChanged(car);
        return car.getId();
    }

    /**
     * metoda care face update la masina cu un anumit nume
     * @param id
     * @param carDTO
     * @return CarDetailsDTO
     */
    public CarDetailsDTO update(UUID id, CarDetailsDTO carDTO) {
        Optional<Car> carOptional = carRepository.findById(id);
        if (!carOptional.isPresent()) {
            LOGGER.error("Car with id {} was not found in db", id);
            throw new ResourceNotFoundException(Car.class.getSimpleName() + " with id: " + id);
        }
        Car d = carOptional.get();
        d.setBrand(carDTO.getBrand());
        d.setModel(carDTO.getModel());
        d.setEngine(carDTO.getEngine());
        d.setType(carDTO.getType());
        d.setPrice(carDTO.getPrice());
        carRepository.save(d);
        return CarBuilder.toCarDetailsDTO(d);
    }

    /**
     * metoda care sterge masina cu un anumit nume
     * @param id
     * @return "delete succesful" message
     */
    public String delete(UUID id){
        carRepository.deleteById(id);
        return "Delete succsessful";
    }
}
